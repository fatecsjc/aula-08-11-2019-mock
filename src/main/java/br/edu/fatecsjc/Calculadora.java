package br.edu.fatecsjc;

public interface Calculadora {

    Float somar(Float x, Float y);

    Float subtrair(Float x, Float y);

    Float multiplicar(Float x, Float y);

    Float dividir(Float x, Float y);
}

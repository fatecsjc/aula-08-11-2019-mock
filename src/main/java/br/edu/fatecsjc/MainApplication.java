package br.edu.fatecsjc;

import org.mockito.Mockito;

public class MainApplication {

    public static void main(String[] args) {

        Calculadora calculadora = Mockito.mock(Calculadora.class);

        Mockito.when(calculadora.somar(1f, 1f)).thenReturn(2f);
        System.out.println("Soma: " + calculadora.somar(1f, 1f));

        Mockito.when(calculadora.subtrair(2f, 1f)).thenReturn(1f);
        System.out.println("Subtração: " + calculadora.subtrair(2f, 1f));

        Mockito.when(calculadora.multiplicar(2f, 2f)).thenReturn(4f);
        System.out.println("Multiplicação: " + calculadora.multiplicar(2f, 2f));

        Mockito.when(calculadora.dividir(4f, 2f)).thenReturn(2f);
        System.out.println("Divisão: " + calculadora.dividir(4f, 2f));
    }
}
